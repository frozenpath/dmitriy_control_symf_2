<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsersData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $user1 = new User();
        $user2 = new User();
        $user3 = new User();

        $comment1 = new Comment();
        $comment2 = new Comment();
        $comment3 = new Comment();
        $comment4 = new Comment();
        $comment5 = new Comment();
        $comment6 = new Comment();
        $comment7 = new Comment();
        $comment8 = new Comment();

        $photo1 = new Photo();
        $photo2 = new Photo();
        $photo3 = new Photo();
        $photo4 = new Photo();
        $photo5 = new Photo();
        $photo6 = new Photo();
        $photo7 = new Photo();
        $photo8 = new Photo();

        $admin
            ->setEmail('admin@admin.ru')
            ->setUsername('admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true)
            ->setName('Злой Админ');
        $encoder = $this->container->get('security.password_encoder');
        $password1 = $encoder->encodePassword($admin, '123');
        $admin->setPassword($password1);

        $user1
            ->setEmail('froz@13@mail.ru')
            ->setUsername('Frozenpath')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true)
            ->setName('Дмитрий');
        $encoder = $this->container->get('security.password_encoder');
        $password2 = $encoder->encodePassword($user1, '123');
        $user1->setPassword($password2);

        $user2
            ->setEmail('froz@213@mail.ru')
            ->setUsername('Wolf')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true)
            ->setName('Ислам');
        $encoder = $this->container->get('security.password_encoder');
        $password3 = $encoder->encodePassword($user2, '123');
        $user2->setPassword($password3);

        $user3
            ->setEmail('froz@143@mail.ru')
            ->setUsername('Wizard')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true)
            ->setName('Владимир');
        $encoder = $this->container->get('security.password_encoder');
        $password4 = $encoder->encodePassword($user3, '123');
        $user3->setPassword($password4);

        $photo1
            ->setPhotoName('photo1.jpg')
            ->setTitle('Фото-1')
            ->setUser($user1);

        $photo2
            ->setPhotoName('photo2.jpg')
            ->setTitle('Фото-2')
            ->setUser($user3);

        $photo3
            ->setPhotoName('photo3.jpg')
            ->setTitle('Фото-3')
            ->setUser($user2);

        $photo4
            ->setPhotoName('photo4.jpg')
            ->setTitle('Фото-4')
            ->setUser($user1);

        $photo5
            ->setPhotoName('photo5.jpg')
            ->setTitle('Фото-5')
            ->setUser($user3);

        $photo6
            ->setPhotoName('photo6.jpg')
            ->setTitle('Фото-6')
            ->setUser($user2);

        $photo7
            ->setPhotoName('photo7.jpg')
            ->setTitle('Фото-7')
            ->setUser($user1);

        $photo8
            ->setPhotoName('photo8.jpg')
            ->setTitle('Фото-8')
            ->setUser($user1);

        $comment1
            ->setUser($user1)
            ->setScore('5')
            ->setPhoto($photo1)
            ->setComment('Комментарий-1');

        $comment2
            ->setUser($user1)
            ->setScore('3')
            ->setPhoto($photo1)
            ->setComment('Комментарий-2');

        $comment3
            ->setUser($user2)
            ->setScore('4')
            ->setPhoto($photo1)
            ->setComment('Комментарий-3');

        $comment4
            ->setUser($user2)
            ->setScore('1')
            ->setPhoto($photo3)
            ->setComment('Комментарий-4');

        $comment5
            ->setUser($user3)
            ->setScore('3')
            ->setPhoto($photo1)
            ->setComment('Комментарий-5');

        $comment6
            ->setUser($user3)
            ->setScore('4')
            ->setPhoto($photo8)
            ->setComment('Комментарий-6');

        $comment7
            ->setUser($user1)
            ->setScore('5')
            ->setPhoto($photo8)
            ->setComment('Комментарий-7');

        $comment8
            ->setUser($user1)
            ->setScore('4')
            ->setPhoto($photo8)
            ->setComment('Комментарий-8');


        $manager->persist($admin);
        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($photo1);
        $manager->persist($photo2);
        $manager->persist($photo3);
        $manager->persist($photo4);
        $manager->persist($photo5);
        $manager->persist($photo6);
        $manager->persist($photo7);
        $manager->persist($photo8);
        $manager->persist($comment1);
        $manager->persist($comment2);
        $manager->persist($comment3);
        $manager->persist($comment4);
        $manager->persist($comment5);
        $manager->persist($comment6);
        $manager->persist($comment7);
        $manager->persist($comment8);
        $manager->flush();
    }
}
