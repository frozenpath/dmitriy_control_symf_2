<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Photo;
use AppBundle\Form\CommentType;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $user = $this->getUser();

        $photos = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->findAll();


        return $this->render('AppBundle:Site:index.html.twig', array(
            'photos' => $photos,
            'user' => $user
        ));
    }

    /**
     * @Method({"POST", "GET"})
     * @Route("/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $photo = new Photo();

        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photo->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
        }
        return $this->redirectToRoute('app_user_profile', ['user_id' => $user->getId()]);
    }

    /**
     * @Method("GET")
     * @Route("/photo/{photo_id}")
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function photoAction(int $photo_id)
    {
        $user = $this->getUser();
        $scores = [];

        $comment_form = $this->createForm(CommentType::class);

        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($photo_id);

        $average_score = $this->getAverageScore($photo_id, $scores);

        return $this->render('@App/Site/photo.html.twig', array(
            'user' => $user,
            'photo' => $photo,
            'comment_form' => $comment_form->createView(),
            'average_score' => $average_score
        ));
    }

    /**
     * @Method("POST")
     * @Route("/{photo_id}/new-comment")
     * @param Request $request
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newCommentAction(Request $request, int $photo_id)
    {
        $comment = new Comment();
        $comment_form = $this->createForm(CommentType::class, $comment);

        $photo = $this->getDoctrine()
            ->getRepository(Photo::class)
            ->find($photo_id);

        $comment_form->handleRequest($request);

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {
            $comment->setUser($this->getUser());
            $comment->setPhoto($photo);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
        }
        return $this->redirectToRoute('app_site_photo', [
            'photo_id' => $photo->getId()]);
    }

    /**
     * @param int $photo_id
     * @param $scores
     * @return float|int
     */
    public function getAverageScore(int $photo_id, $scores)
    {
        $average_score = null;
        $comments = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findBy(['photo' => $photo_id]);

        foreach ($comments as $comment) {
            $scores[] = $comment->getScore();
        }

        $count = count($scores);

        if ($count > 0) {
            $average_score = array_sum($scores) / $count;
        }
        return number_format($average_score, 1);
    }

    /**
     * @Route("/photo/delete/{photo_id}")
     * @Method("DELETE")
     * @param $photo_id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deletePhotoAction(int $photo_id)
    {
        $user = $this->getUser();

        $photo = $this->getDoctrine()
            ->getRepository(Photo::class)
            ->find($photo_id);

        $comments = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->findBy(['photo' => $photo_id]);

        $em = $this->getDoctrine()->getManager();

        foreach ($comments as $comment) {
            $em->remove($comment);
        }

        $em->remove($photo);
        $em->flush();

        return $this->redirectToRoute('app_user_profile', ['user_id' => $user->getId()]);
    }
}
