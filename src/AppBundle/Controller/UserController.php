<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\DeleteType;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/user/{user_id}")
     * @param int $user_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(int $user_id)
    {
        $user = $this->getUser();

        $photo_form = $this->createForm(PhotoType::class);

        $delete_form = $this->createForm(DeleteType::class);

        /** @var User $user */

        $profile = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($user_id);

        $photos = $profile->getPhotos();

        return $this->render('@App/User/profile.html.twig', array(
            'profile' => $profile,
            'photos' => $photos,
            'user' => $user,
            'photo_form' => $photo_form->createView(),
            'delete_form' => $delete_form
        ));
    }
}
